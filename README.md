# Mapas conceptuales
## 1. Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)
```plantuml
@startmindmap
caption Mapa conceptual 1/3
title Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)
header
Cruz Morales Jesús
endheader
<style>
 node {
  Padding 12
  Margin 3
  HorizontalAlignment center
  LineColor lightblue
  LineThickness 1.0
  BackgroundColor lightblue
  RoundCorner 40
  MaximumWidth 100
 }

 arrow{
  LineColor black
 }
</style>
* <b>Sistemas
 *_ se define\ncomo
  * Sistema\nactual
   *_ son los\nque
    * Están a\nla venta
   *_ sobre la
    * Arquitectura
     *_ el\nprogramador
      * Desconoce
     *_ son
      * Parecidas
   *_ utiliza\nprogramación
    * En capas
     *_ permite\ncrear
      * Software\ncomplejo
       *_ a través\nde un
        * Mayor nivel\nde abstracción
         *_ ocasiona
          * Desconocimiento\nde código útil
           *_ por\nejemplo
            * Caso\nleftpad
         *_ incluye
          * Sobrecarga
           *_ comunmente
            * Llamadas al\nsistemas
       *_ pero es\nsoftware
        * Ineficiente
         *_ por exceso\nde
          * Bugs
           *_ ocasionado\npor el
            * Programador
            * Software
  * Sistema\nantigüo
   *_ estos
    * No están\na la venta
   *_ la
    * Arquitectura
     *_ el programador\ndebía
      * Conocer\na fondo
     *_ trataban\nde hacerlas
      * Distintas
   *_ utiliza\nprogramación
    * Imperativa
     *_ permitía\ncrear
      * Software\nbásico
       *_ altamente
        * Eficiente
         *_ por su
          * Rapidez
       *_ trabajaba\ncon
        * Componentes\ndel ordenador
         *_ por lo que el\nprogramador debía
          * Conocer a fondo\nel código útil
@endmindmap
```
## 2. Hª de los algoritmos y de los lenguajes de programación (2010)
```plantuml
@startmindmap
caption Mapa conceptual 2/3
title Hª de los algoritmos y de los lenguajes de programación (2010)
header
Cruz Morales Jesús
endheader
<style>
 node {
  Padding 12
  Margin 3
  HorizontalAlignment center
  LineColor #87CDDE
  LineThickness 1.0
  BackgroundColor #87CDDE
  RoundCorner 40
  MaximumWidth 100
 }

 arrow{
  LineColor black
 }
</style>
 * <b>Algoritmo
  *_ es un
   * Procedimiento\nsistemático y mecánico
    *_ permite\nhallar
     * Soluciones
      *_ a\nun
       * Problema
  *_ su\nimportancia
   * Mostrar
    *_ como llevar\na cabo un
     * Proceso de resolución
  *_ sus
   * Límites
    *_ están dados por
     * Teoremas\nmatemáticos
  *_ se clasifica\npor
   * Costo
    *_ estos\npueden ser
     * Razonables
      *_ el tiempo\nde ejecución
       * Crece\ndespacio
        *_ mientras
         * Aumenta el\nproblema
     * No razonables
      *_ estos
       * Crecen\ndeprisa
        *_ no alcanzan la
         * Ejecución
  *_ su complejidad
   * Conjunto de\nproblemas
    *_ que se\nresuelve en
     * Tiempo
      * Polinomial
       *_ se llama
        * P
      * No polimonial
       *_ se llama
        * NP
  *_ algo de\nhistoria
   * Mesopotamia
    *_ uso para
     * Cálculos para transacciones comerciales
   * Siglo XVII
    *_ aparecen las\nprimeras
     * Ayudas\nmecánicas
   * Siglo XIX
    *_ aparecen las\nprimeras
     * Máquinas\nprogramables
   * Siglo XX
    *_ aparecen\nlos
     * Ordenadores
      *_ ocasiona\nun
       * Desarrollo sin\nprecedente de\nalgoritmos
      *_ surgen por\nnecesidad de 
       * Mecanizar\nalgoritmos
      *_ consigo los
       * Lenguajes de\nprogramación
        *_ pensado\ncomo
         * Instrumento para comunicar\nlos algoritmos con las máquinas
        *_ traen\nconsigo
         * Programas
          *_ son\nun
           * Conjunto de\noperaciones
            *_ de un\nmismo
             * Lenguaje
        *_ aparecen\nlos
         * Paradigmas
          *_ algunos\nson
           * Imperativa
            *_ consiste
             * Secuencia de ordenes\na la máquina
           * Funcional
            *_ basado\nen
             * Simplificar expresiones
           * Orientado\na Objetos
            *_ visto\ncomo
             * Células que\nse comunican
           * Lógico
            *_ utilización de
             * Expresiones\nlógicas
@endmindmap
```
## 3. Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)
```plantuml
@startmindmap
caption Mapa conceptual 3/3
title Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)
header
Cruz Morales Jesús
endheader
<style>
 node {
  Padding 12
  Margin 3
  HorizontalAlignment center
  LineColor skyblue
  LineThickness 1.0
  BackgroundColor skyblue
  RoundCorner 40
  MaximumWidth 100
 }
 
 *{
  FontColor white
 }

 arrow{
  LineColor black
 }
</style>
 * <b>La evolución de los\n<b>lenguajes y paradigmas
  * Ordenador
   *_ facilita el
    * Trabajo intelectual
  * Lenguaje de\nprogramación
   *_ medio de comunicación\nentre el
    * Hombre
     *_ y la 
      * Máquina
  * Paradigma
   *_ se puede\nentender como
    * Forma de aproximarse
     *_ a la
      * Solución de un problema
   *_ surge como\nresultado de
    * Crisis en la programación
    * Fallas
    * Corregir errores
   *_ su
    * Tendencia
     *_ clara tendencia a la
      * Abstracción
     *_ nuevas
      * Formas de pensar
     *_ inclinado a la
      * Arquitectura de software
   *_ algunos\nparadigmas
    * Programación\nestructurada
     *_ utiliza
      * Programas y algoritmos convencionales
     *_ separa\ndependencias de
      * Hardware
      * Estructuras abstractas
      * Concepción de diseño
      * Modulos
    * Paradigma funcional
     *_ utiliza
      * Lenguaje matemático
       *_ basado en
        * Funciones matemáticas
       *_ hace uso de la
        * Recursividad 
     *_ genera programas
      * Correctos y verificados
    * Paradigma\nlógico
     *_ uso de
      * Expresiones\nlógicas
       *_ trabaja con
        * Predicados\nlógicos
         *_ como
          * Mecanismos\nde inferencia
           *_ para\nllegar a la
            * Solución
      *_ usa la recursividad para
       * Simular incidencias
    * Programación concurrente
     *_ pretende dar
      * Solución
       *_ a varios
        * Usuarios
         *_ que acceden
          * Simultaneamente
    * Programación distribuida
     *_ trata de
      * Comunicar\nordenadores
       *_ estos contienen el
        * Programa
         *_ por lo que\neste está
          * Dividido entre\nlos ordenadores
    * Programación Orientada a Objetos
     *_ utiliza el\npensar
      * Abstracto
       *_ por lo que 
        * Se desconoce el\nfuncionamiento individual
         *_ todo es\nparte de un
          * Conjunto
    * Programación de componentes
     *_ visto como un\nconjunto de 
      * Objetos
       *_ por lo que
        * Reutiliza código con facilidad
    * Programación orientada a aspectos
     *_ se trata de\nimplementar
      * Esqueletos(aspectos)
       *_ estos pueden\nser programados
        * Separados
         *_ al final debe
          * Unirse a\nlos demás
    * Programación orientada\na agente software
     *_ utilizado en
      * Inteligencia\nArtificial
       *_ se crean
        * Agentes
         *_ capaces de
          * Actuar
          * Percibir el entorno
          * Alcanzar su objetivo
          * Socializar con otros agentes
@endmindmap
```

